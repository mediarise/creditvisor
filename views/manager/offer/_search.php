<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OfferSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'body') ?>

    <?= $form->field($model, 'slug') ?>

    <?= $form->field($model, 'tags') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'counter') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'stand') ?>

    <?php // echo $form->field($model, 'min_rate') ?>

    <?php // echo $form->field($model, 'max_sum') ?>

    <?php // echo $form->field($model, 'max_term') ?>

    <?php // echo $form->field($model, 'min_age') ?>

    <?php // echo $form->field($model, 'max_age') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
