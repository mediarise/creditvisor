<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%offer}}".
 *
 * @property int $id id
 * @property string $title
 * @property string $body
 * @property string $slug
 * @property string $tags
 * @property string $url
 * @property int $counter
 * @property int $active
 * @property array $data
 * @property string $image
 * @property int $stand
 * @property double $min_rate
 * @property double $max_sum
 * @property double $max_term
 * @property int $min_age
 * @property int $max_age
 */
class Offer extends \yii\db\ActiveRecord
{


    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%offer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['body', 'tags'], 'string'],
            [['counter', 'active', 'stand', 'min_age', 'max_age'], 'integer'],
            [['data'], 'safe'],
            [['min_rate', 'max_sum', 'max_term'], 'number'],
            [['title', 'image'], 'string', 'max' => 100],
            [['slug'], 'string', 'max' => 60],
            [['url'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'id'),
            'title' => Yii::t('app', 'Title'),
            'body' => Yii::t('app', 'Body'),
            'slug' => Yii::t('app', 'Slug'),
            'tags' => Yii::t('app', 'Tags'),
            'url' => Yii::t('app', 'Url'),
            'counter' => Yii::t('app', 'Counter'),
            'active' => Yii::t('app', 'Active'),
            'data' => Yii::t('app', 'Data'),
            'image' => Yii::t('app', 'Image'),
            'stand' => Yii::t('app', 'Stand'),
            'min_rate' => Yii::t('app', 'Min Rate'),
            'max_sum' => Yii::t('app', 'Max Sum'),
            'max_term' => Yii::t('app', 'Max Term'),
            'min_age' => Yii::t('app', 'Min Age'),
            'max_age' => Yii::t('app', 'Max Age'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->upload();
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     * @return OfferQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OfferQuery(get_called_class());
    }

    public function upload()
    {

        if ($this->validate()) {

            if (!empty($this->imageFile)) {
                $filename = Yii::$app->getSecurity()->generateRandomString(15);
                $this->imageFile->saveAs(Yii::getAlias('@webroot') . '/uploads/' . $filename . '.' . $this->imageFile->extension);
                $this->image = $filename . '.' . $this->imageFile->extension;
            }
            return true;
        } else {
            return false;
        }
    }
}
