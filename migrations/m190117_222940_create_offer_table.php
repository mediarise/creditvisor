<?php

use yii\db\Migration;

/**
 * Handles the creation of table `offer`.
 */
class m190117_222940_create_offer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('offer', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'body' => $this->text(),
            'slug' => $this->string(100),
            'tags' => $this->string(255),
            'url' => $this->string(255),
            'counter' => $this->integer(11),
            'active' => $this->integer(1),
            'data' => $this->text(),
            'image' => $this->string(255),
            'stand' => $this->integer(11),
            'min_rate' => $this->float(),
            'max_sum' => $this->float(),
            'max_term' => $this->float(),
            'min_age' => $this->integer(3),
            'max_age' => $this->integer(3),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('offer');
    }
}
